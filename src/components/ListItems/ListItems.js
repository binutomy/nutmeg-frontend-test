import React , { Component } from "react";
import ListItem from "../ListItem/ListItem";

class ListItems extends Component {

    constructor(props) {
        super(props);
        this.state = {
          selectedOption: ""
        };
    }

    _handleOptionChange = (changeEvent) => {
        this.setState({
            selectedOption: changeEvent.target.value
        });
    }

    _numberFormatting = (x) => { 
        const number = parseFloat(x).toFixed(2)
        const withCommas = Number(number).toLocaleString('en');
        return withCommas;
    }

    render() {
        return(
            <section className="card">
               <ul>
                    {<ListItem 
                        pots={this.props}
                        handleOptionChange={this._handleOptionChange}
                        numberFormatting = {this._numberFormatting}
                        selectedOption= {this.state.selectedOption} />}
               </ul>
            </section>

        )
    }
}

export default ListItems;