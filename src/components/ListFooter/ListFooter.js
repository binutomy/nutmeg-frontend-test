import React , { Component } from "react";
import Button from "../Button/Button";

class ListFooter extends Component {
      
      _handleClick = (e) => { 
        e.preventDefault()
      }

    render() {
        return(
            <section className="pot-footer">
                <Button onClick={this._handleClick} labelText="back" className="btn-default btn-padding"> Back </Button>
                <Button onClick={this._handleClick} labelText="next" className="btn-success btn-padding"> Next</Button>
            </section>

        )
    }
}

export default ListFooter;