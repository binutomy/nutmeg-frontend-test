import React, { Component } from "react";
import Header from './components/Header/Header';
import List from './components//List/List';
import { getData } from './utils/api';
import './css/style.css'

class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
      data: null,
      error: null,
    };
  }
  
  componentDidMount() {
    //Fetching using the getData api in Utils and saving the data to state
    getData('http://localhost:3004/pots')
      .then(data => this.setState({
        data
      }))
      .catch(error => this.setState({
        error
      }));
  }

  render() {
    //filtering data to only show the isWithdrawable, the child components will only need to display them
    var data = this.state.data ? this.state.data.filter( item => item.isWithdrawable) : [];
    const loading = <div className="pot--wrapper container">
                      <h4>Loading...</h4>
                    </div>;
    
    return (
      <div className="App" role='Main'>
        <Header />
        {/* If data is not returned, just have a loading screen */}
        {this.state.error ?  
          loading : 
          <List data={data} />
        }
      </div>
    );
  }
}

export default App;
