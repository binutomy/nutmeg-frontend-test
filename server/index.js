var express = require('express');
var cors = require('cors');
var app = express();
var activePots = require('./pots.json');

var SERVER_PORT = 3004;

app.use(cors({
  origin: "*"
}));

app.get('/pots', function(request, response) {
  response.setHeader('Content-Type', 'application/json');
  response.send(JSON.stringify(activePots));
});

app.listen(SERVER_PORT, () => console.log('Pot service listening on http://localhost:' + SERVER_PORT));