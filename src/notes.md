### Structure

Initially i started with a file structure like below, but only to then realise it was overcomplicating things. Also using BEM wouldnt really work this way
```
src
├── App
│   ├── component.css
│   ├── component.js
│   └── index.js
├── Checkbox
│   ├── component.css
│   ├── component.js
│   └── index.js
├── Data
│   └── index.js
├── OptionsList
│   ├── component.css
│   ├── component.js
│   └── index.js
└── index.js
```

So i took a step back to the drawing board and came up with something simpler
```
src
├── Components
|   |── Header (the original header from the test)
│   ├── List (which is a wrapper component)
│   ├── ListHeader (Component for the title )
│   ├── ListItem ( where each pot will sit in a list with a radio button) 
|   |── ListItems ( wrapper for the listItems, also to keep things like numberformatting) 
│   |── ListFooter  ( where the button would be used)
│   └── Button (simply rendering the buttons)
├── Utils
│   ├── Api.js
|
└── App.js
```

Even with this approach instead of just having `js` files inside components i went to have its own individual folder so that in the future if i was to write test for this, i can just add `button.test.js` for the test. Maybe even break down the css so each component would have its own css. Like below

```
Components
├── Buttons
|   |── Button.js 
|   |── Button.test.js 
│   └── button.css
```

## NumberFormater
Its what i was using at the beginning. 
This felt a bit of an overkill, but its a suggested method on Stackoverflow
```
function numberFormat(x) {
    x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",")
}
```

