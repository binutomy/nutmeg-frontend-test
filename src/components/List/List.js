import React , { Component } from "react";
import ListHeader from "../ListHeader/ListHeader";
import ListFooter from "../ListFooter/ListFooter";
import ListItems from "../ListItems/ListItems";

class List extends Component {
    
    render() {
        return(
            <article className="pot--wrapper container">
                <ListHeader />
                <ListItems data={this.props.data}/>
                <ListFooter />
            </article>
        )
    }
}

export default List;