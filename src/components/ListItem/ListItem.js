import React from "react";

const ListItem = (props) => {

    const potList = props.pots.data.map( item => {
                    return (
                    <li key={item.id} className="card__item">
                        
                        <input type="checkbox" 
                            className="check--box"
                            value={item.name}
                            id={item.name}
                            checked={props.selectedOption === item.name}
                            onChange={props.handleOptionChange} /> 
                        <label className="check--box__label" htmlFor={item.name}></label>
                        <div className="card__wrapper">
                            <span className="card__title">{item.name}</span>
                            <p className="card__text">£{props.numberFormatting(item.value)} <span>available</span></p>
                        </div>
                        
                    </li>
                    )
                })
    return (
            <div>
                <ul>
                    {potList}
                </ul>
            </div>        
        );
};
      
export default ListItem;