import React from "react";

const Button = ({children, className, onClick, labelText}) => {
    return (
    <button 
        className={`btn ${className}`}
        onClick = {onClick}
        aria-label={labelText} 
        >
        {children}
    </button>
    );
  };
  
  export default Button;
