# Nutmeg Frontend Coding Test

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Setup

Run

```
npm install
```

In the project directory in a separate terminal, first run:

```
npm run server
```

Runs the server so you can consume the array of pot objects
Open [http://localhost:3004/pots](http://localhost:3004/pots) to see the payload.

Then run:

```
npm start
```

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.


```
npm run build-css
```

If you need to add any more css, please add it into the SASS folder and it will compile into css.

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.
