import React from "react";

const ListHeader = () => {
    return (
        <div className="pot--header">
            <h4 className="pot__heading">Pick a pot</h4>
            <h6 className="pot__subheading">Please choose a pot to withdraw from</h6>
        </div>
    )
}

export default ListHeader;
